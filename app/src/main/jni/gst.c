#include <jni.h>
#include <string.h>
#include <gst/gst.h>


jstring
Java_leonardo_ferraro_gstreamertest_MainActivity_getGstInfo(JNIEnv *env, jobject obj)
{
	char *version_utf8 = gst_version_string();
	jstring *version_jstring = (*env)->NewStringUTF(env, version_utf8);
	g_free (version_utf8);
	return version_jstring;
}