package ferraro.leonardo.gstreamertest;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {

    private TextView gst_TV;
    private native String getGstInfo();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        try {
            GStreamer.init(this);
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gst_TV = (TextView)this.findViewById(R.id.gstTV);
        gst_TV.setText("Versione Gstreamer :" + getGstInfo());

    }

    static {
        System.loadLibrary("gstreamer_android");
        System.loadLibrary("gst");
    }

}
